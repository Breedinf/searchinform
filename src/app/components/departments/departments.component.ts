import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Department} from '../../models/department';
import {DataProviderService} from '../../services/data-provider.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DepartmentsComponent implements OnInit {

  constructor(private dataProviderService: DataProviderService,
              private router: Router) { }
  public departments: Department[];
  ngOnInit() {
    this.dataProviderService.getDepartments()
      .then(departments => {
        this.departments = departments;
      });
  }

  onSelect({ selected }) {
    this.router.navigateByUrl('/departments/' + selected[0].id + '/employees');
  }

  getRowClass(row) {
    return {
      'departments__row': true
    };
  }

}
