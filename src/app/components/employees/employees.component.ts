import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataProviderService} from '../../services/data-provider.service';
import {Employee} from '../../models/employee';
import {ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmployeesComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dataProviderService: DataProviderService) { }
  public departmentId: number;
  private subscribe: any;
  public employees: Employee[];

  ngOnInit() {
    this.subscribe = this.route.params.subscribe(params => {
      this.departmentId = +params['id'];
      this.refreshData();
    });
  }

  refreshData() {
    this.dataProviderService.getEmployees(this.departmentId)
      .then(employees => {
        this.employees = employees;
    });
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }

  onSelect({ selected }) {
    this.router.navigateByUrl('/employees/' + selected[0].id);
  }

  getRowClass(row) {
    return {
      'employees__row': true
    };
  }


}
