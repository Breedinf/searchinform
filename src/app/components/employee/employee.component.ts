import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataProviderService} from '../../services/data-provider.service';
import {ActivatedRoute} from '@angular/router';
import {Employee} from '../../models/employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute,
              private dataProviderService: DataProviderService) { }

  private subscribe: any;
  public employee: Employee;
  public selectedFile;
  public showUploadSuccess = false;
  public alertText: string = "Upload Success!";

  ngOnInit() {
    this.subscribe = this.route.params.subscribe(params => {
      this.refreshData(params['id']);
    });
  }

  refreshData(id: number) {
    this.dataProviderService.getEmployee(id)
      .then(employee => {
        this.employee = employee;
      });
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }

  onFileChanged(ev) {
    this.selectedFile = ev.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onloadend = () => {
      this.employee.img = {
        name: this.selectedFile.name,
        data: reader.result
      };
      this.employee.photo = reader.result;
    };
  }

  onUpload(ev) {
    this.dataProviderService.updateEmployee(this.employee)
      .then(e => {
        this.alertText = e ? "Upload succeed!" : "Photo stored, but not uploaded";
        this.showUploadSuccess = true;
        setTimeout(() => this.showUploadSuccess = false, 3000);
      });
  }

  getSrc() {
    return (this.employee && this.employee.photo) ? this.employee.photo : '/assets/img/no-avatar.png';
  }

}
