import { Component, OnInit } from '@angular/core';
import {BufferService} from '../../../services/buffer/buffer.service';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {

  public text: string;
  public show: boolean = false;

  constructor(public buffer: BufferService) {
    this.buffer.onFlushCompleted$.subscribe((val) => {
      if (val) {
        this.show = true;
        this.text = "All stored data successfully saved";
        console.log("ALL STORED");
        setInterval(() => {
          this.show = false;
        }, 5000);
      }
    });
  }

  ngOnInit() {
  }

}
