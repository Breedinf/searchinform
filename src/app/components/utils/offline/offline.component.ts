import { Component, OnInit } from '@angular/core';
import {BufferService} from '../../../services/buffer/buffer.service';
import {DataProviderService} from '../../../services/data-provider.service';

@Component({
  selector: 'app-offline',
  templateUrl: './offline.component.html',
  styleUrls: ['./offline.component.scss']
})
export class OfflineComponent implements OnInit {

  public isOffline: boolean = false;
  public text: string = "You are offline";

  constructor(public dataProviderService: DataProviderService) {

    this.dataProviderService.onOffline$.subscribe(() => {
      this.isOffline = true;
      this.text = "You are offline";
    });

    this.dataProviderService.onOnline$.subscribe(() => {
      if (this.isOffline) {
        setTimeout(() => {
          this.isOffline = false;
        }, 3000);
        this.text = "You are online";
      }
    });
  }

  ngOnInit() {
  }

}
