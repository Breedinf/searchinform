import { Injectable } from '@angular/core';
import {Department} from '../models/department';
import {classToPlain, deserializeArray, plainToClass, serialize} from 'class-transformer';
import {DepartmentsService} from './api-services/departments.service';
import {EmployeesService} from './api-services/employees.service';
import {Employee} from '../models/employee';
import {BufferService} from './buffer/buffer.service';
import {BaseService} from './api-services/base.service';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataProviderService extends BaseService{

  public onOnline$: BehaviorSubject<void> = new BehaviorSubject<void>(null);
  public onOffline$: BehaviorSubject<void> = new BehaviorSubject<void>(null);

  constructor(private departmentsService: DepartmentsService,
              private employeesService: EmployeesService,
              public http: HttpClient,
              private buffer: BufferService) {
    super(http);

    setInterval(() => {
      this.ping()
        .then(() => this.onOnline$.next(null))
        .catch(() => this.onOffline$.next(null));
    }, 10000);
  }

  private DEPARTMENTS_LOCAL_STORAGE_KEY = 'departments';

  private getCachedDepartments(): Department[] {
    const departmentsData = localStorage.getItem('departments');
    if (departmentsData !== null) {
      try {
        const departments: Department[] = deserializeArray(Department, departmentsData);
        return departments;
      } catch (e) {
        console.error('no local storage data or local storage data corrupted');
      }
    }
    return null;
  }

  public getDepartments(): Promise<Department[]> {
    return this.departmentsService.getDepartments()
        .then(deps => {
          localStorage.setItem(this.DEPARTMENTS_LOCAL_STORAGE_KEY, JSON.stringify(deps));
          return deps;
        })
        .catch(error => {
          return this.getCachedDepartments();
        });
  }

  public getEmployees(departmentId: number): Promise<Employee[]> {
    return this.departmentsService.getEmployees(departmentId)
      .then(employees => {
        return employees;
      })
      .catch(error => {
        console.warn('OFFLINE MODE');
        const departments: Department[] = this.getCachedDepartments();
        const department = departments.find(d => d.id === departmentId);
        return Promise.resolve(department ? department.employees : []);
      });
  }

  public getEmployee(id: number): Promise<Employee> {
    return this.employeesService.getEmployee(id)
      .then(employee => {
        return employee;
      })
      .catch(error => {
        console.warn('OFFLINE MODE');
        const departments: Department[] = this.getCachedDepartments();
        let employee: Employee = null;
        if (departments instanceof Array) {
          departments.forEach(d => d.employees.forEach(e => {
            if (e.id == id) {
              employee = e;
            }
          }));
          return Promise.resolve(employee);
        }
        else {
          console.error("No cached data");
        }
      });
  }

  public updateEmployee(emp: Employee): Promise<void|Employee> {
    return this.employeesService.updateEmployee(emp)
      .then(employee => {
        return employee;
      })
      .catch(error => {
        this.buffer.add(emp);
        console.warn('OFFLINE MODE');
        const departments: Department[] = this.getCachedDepartments();
        departments.forEach(d => {
          const index = d.employees.findIndex(e => e.id == emp.id);
          if (index !== -1) {
            d.employees.splice(index, 1, emp);
          }
        });
        localStorage.setItem(this.DEPARTMENTS_LOCAL_STORAGE_KEY, JSON.stringify(departments));
      });
  }
}
