import { Injectable } from '@angular/core';
import {Department} from '../../models/department';
import {classToPlain, plainToClass} from 'class-transformer';
import {BaseService} from './base.service';
import {Employee} from '../../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService extends BaseService {

  public getEmployee(id: number): Promise<Employee> {
    return this.executeGet<Employee>(`employees/${id}`, null)
      .then(data => plainToClass(Employee, data));
  }

  public updateEmployee(employee: Employee): Promise<Employee> {
    return this.executePost(`employees/${employee.id}`, JSON.stringify(employee))
      .then(data => plainToClass(Employee, data));
  }
}
