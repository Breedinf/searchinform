import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  private BACKEND_URL = `${window.location.protocol}//${environment.MAIN_HOST}/api/`;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(public http: HttpClient) {}

  public executeGet<T>(url: string, options?: {}): Promise<T> {
    return this.http.get<T>(this.BACKEND_URL + url, Object.assign(this.httpOptions, options)).toPromise<T>();
  }

  public executePost<T>(url: string, data: any, options?: {}): Promise<T> {
    return this.http.post<T>(this.BACKEND_URL + url, data, Object.assign(this.httpOptions, options)).toPromise<T>();
  }

  public executePut<T>(url: string, data: any, options?: {}): Promise<T> {
    return this.http.put<T>(this.BACKEND_URL + url, data, Object.assign(this.httpOptions, options)).toPromise<T>();
  }

  public executeDelete<T>(url: string, options?: {}): Promise<T> {
    return this.http.delete<T>(this.BACKEND_URL + url, Object.assign(this.httpOptions, options)).toPromise<T>();
  }

  public ping(): Promise<any> {
    return this.http.get(this.BACKEND_URL + 'ping', {}).toPromise();
  }
}
