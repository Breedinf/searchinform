import { Injectable } from '@angular/core';
import {Department} from '../../models/department';
import {BaseService} from './base.service';
import {plainToClass} from 'class-transformer';
import {Employee} from '../../models/employee';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService extends BaseService {

  public getDepartments(): Promise<Department[]> {
    return this.executeGet<Department[]>('departments')
      .then(data => plainToClass(Department, data) );
  }

  public getEmployees(departmentId: number): Promise<Employee[]> {
    return this.executeGet<Employee[]>(`departments/${departmentId}/employees`)
      .then(data => plainToClass(Employee, data));
  }
}
