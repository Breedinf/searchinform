import { Injectable } from '@angular/core';
import {EmployeesService} from '../api-services/employees.service';
import {Employee} from '../../models/employee';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BufferService {

  constructor(private employerService: EmployeesService,
              private http: HttpClient) {
    setInterval(() => {
      this.flush();
    }, 10000);

  }
  private saveBuffer: Employee[] = [];
  public onFlushCompleted$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  add(employee: Employee) {
    let id = this.saveBuffer.findIndex(e => e.id == employee.id);
    if (id != -1) {
      this.saveBuffer.splice(id, 1, employee);
    }
    else {
      this.saveBuffer.push(employee)
    }
  }

  flush() {
    let promises = [];
    this.saveBuffer.forEach(e => {
      promises.push(this.employerService.updateEmployee(e)
        .then(saved => {
          let id = this.saveBuffer.findIndex(e => e.id == saved.id);
          if (id != -1) {
            this.saveBuffer.splice(id, 1);
          }
        })
        .catch(error => {console.warn("Still offline")}));
    });
    if (promises.length > 0) {
      Promise.all(promises)
        .then((values) => {
          if (this.saveBuffer.length == 0) {
            this.onFlushCompleted$.next(true);
          }
        });
    }
  }
}
