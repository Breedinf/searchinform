import {EmployeesService} from '../services/api-services/employees.service';

export class Employee {

  constructor(private employeeService: EmployeesService){}

  id: number;
  name: String;
  photo: String;
  phone: String;
  img: any;
  departmentId: number;
}
