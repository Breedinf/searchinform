import {Employee} from './employee';
import {Type} from 'class-transformer';

export class Department {
  id: number;
  name: String;

  @Type(() => Employee)
  employees: Employee[] = [];
  get employeesCount() {
    return this.employees.length;
  }
}
