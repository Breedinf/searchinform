import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DepartmentsComponent } from './components/departments/departments.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { NotFoundComponent } from './components/utils/not-found/not-found.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {DepartmentsService} from './services/api-services/departments.service';
import {EmployeesService} from './services/api-services/employees.service';
import {DataProviderService} from './services/data-provider.service';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {HeaderInterceptor} from './services/api-services/interceptor';
import { OfflineComponent } from './components/utils/offline/offline.component';
import { AlertsComponent } from './components/utils/alerts/alerts.component';
import {Employee} from './models/employee';

const appRoutes: Routes = [
  { path: 'departments', component: DepartmentsComponent},
  { path: 'departments/:id/employees', component: EmployeesComponent},
  { path: 'employees/:id', component: EmployeeComponent },
  { path: '', redirectTo: '/departments', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DepartmentsComponent,
    EmployeesComponent,
    EmployeeComponent,
    NotFoundComponent,
    NavigationComponent,
    OfflineComponent,
    AlertsComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    NgxDatatableModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true,
    },
    DepartmentsService, EmployeesService, DataProviderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
